package org.asproject.gui.ihm;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;

public class NumberError extends JFrame {


	public NumberError() {
		setVisible(true);
		getContentPane().setBackground(new Color(255, 248, 220));
		setBounds(100, 100, 400, 200);
		getContentPane().setLayout(null);
		
		JLabel lblErreur = new JLabel("Erreur");
		lblErreur.setFont(new Font("Century Gothic", Font.PLAIN, 13));
		lblErreur.setBounds(176, 17, 61, 16);
		getContentPane().add(lblErreur);
		
		JLabel lblNewLabel = new JLabel("Il n’est possible d’ajouter que 1, 2 ou 3 capteurs à la fois.");
		lblNewLabel.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		lblNewLabel.setBounds(18, 58, 365, 16);
		getContentPane().add(lblNewLabel);
		
		JLabel lblVeuillezVousAssurer = new JLabel("Veuillez vous assurer de bien avoir entré une de ces trois valeurs.");
		lblVeuillezVousAssurer.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		lblVeuillezVousAssurer.setBounds(18, 86, 365, 16);
		getContentPane().add(lblVeuillezVousAssurer);
	}
}
