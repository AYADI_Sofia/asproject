package org.asproject.gui.ihm;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.asproject.commons.Sensor;
import org.asproject.gui.socket.SensorHandler;

import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.awt.Color;

import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Color;

public class Maintenance extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JTextField textField;
	private ResultSet rs;
	private PreparedStatement ps;
	private String st;
	private JButton ok;
	private JButton btnTousLesCapteurs;
	private JRadioButton oneYear;
	private JRadioButton fiveYears;
	private JRadioButton tenYears;
	private int garanty;
	private double anual_maintenance_price;
	private JComboBox type_Capt;
	private JComboBox loc;
	private JComboBox jmarque;
	private String brand;
	private JTextArea coutPourTous;
	private JTextArea coutOk;
	private SensorHandler sh;



	public Maintenance() {

		this.sh = new SensorHandler();


		setVisible(true);
		setBounds(200, 200, 550, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 248, 220));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblMaintenance = new JLabel("Maintenance");
		lblMaintenance.setBounds(214, 6, 101, 17);
		lblMaintenance.setFont(new Font("Century Gothic", Font.BOLD, 13));
		contentPane.add(lblMaintenance);

		JLabel jtype_capt = new JLabel("Type de capteur");
		jtype_capt.setFont(new Font("Century Gothic", Font.BOLD, 11));
		jtype_capt.setBounds(215, 38, 100, 16);
		contentPane.add(jtype_capt);

		type_Capt = new JComboBox();
		type_Capt.setModel(new DefaultComboBoxModel(new String[] {"-- Selectionner --", "detecteur de fumee", "detecteur de mouvement", "capteur de lumiere", "capteur de temperature", "ouverture des portes", "compteur"}));
		type_Capt.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		type_Capt.setBounds(197, 58, 123, 27);
		contentPane.add(type_Capt);

		JLabel jloc = new JLabel("Localisation");
		jloc.setFont(new Font("Century Gothic", Font.BOLD, 11));
		jloc.setBounds(42, 38, 80, 16);
		contentPane.add(jloc);

		loc = new JComboBox();
		loc.setModel(new DefaultComboBoxModel(new String[] {"-- Selectionner --", "couloir", "toilettes", "salle animation collective", "salle activite", "salle de soins", "accueil", "centre operationnel"}));
		loc.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		loc.setBounds(18, 58, 123, 27);
		contentPane.add(loc);

		JLabel lblVoirLePrix = new JLabel("Voir le prix de la maintenance sur :");
		lblVoirLePrix.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblVoirLePrix.setBounds(165, 85, 188, 27);
		contentPane.add(lblVoirLePrix);

		oneYear = new JRadioButton("1 an");
		oneYear.setBounds(95, 116, 59, 23);
		contentPane.add(oneYear);

		fiveYears = new JRadioButton("5 ans");
		fiveYears.setBounds(232, 116, 66, 23);
		contentPane.add(fiveYears);

		tenYears = new JRadioButton("10 ans");
		tenYears.setBounds(344, 116, 80, 23);
		contentPane.add(tenYears);

		ok = new JButton("A l'unité");
		ok.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		ok.setBounds(6, 151, 292, 27);
		contentPane.add(ok);

		coutOk = new JTextArea();
		coutOk.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		coutOk.setColumns(10);
		coutOk.setBounds(5, 178, 292, 58);
		contentPane.add(coutOk);

		coutPourTous = new JTextArea();
		coutPourTous.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		coutPourTous.setColumns(10);
		coutPourTous.setBounds(310, 178, 218, 58);
		contentPane.add(coutPourTous);

		btnTousLesCapteurs = new JButton("Tous les capteurs de la résidence");
		btnTousLesCapteurs.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		btnTousLesCapteurs.setBounds(310, 151, 218, 27);
		contentPane.add(btnTousLesCapteurs);

		jmarque = new JComboBox();
		jmarque.setModel(new DefaultComboBoxModel(new String[] {"-- Selectionner --", "NewOne", "Insafe", "Steihel", "Brennenstuhl", "Iduino", "Microbot", "PyroControle", "Adafruit", "ArtMotion", "Merkur", "Powermaster", "Silamp"}));
		jmarque.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		jmarque.setBounds(380, 58, 123, 27);
		contentPane.add(jmarque);

		JLabel lblMarqueDuCapteur = new JLabel("Marque du capteur");
		lblMarqueDuCapteur.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblMarqueDuCapteur.setBounds(391, 38, 100, 16);
		contentPane.add(lblMarqueDuCapteur);

		ok.addActionListener(this);
		btnTousLesCapteurs.addActionListener(this);
		oneYear.addActionListener(this);
		fiveYears.addActionListener(this);
		tenYears.addActionListener(this);



	}


	public void actionPerformed(ActionEvent e) {
		String type;
		String location; 
		String brand;
		Sensor sensor = null;
		List<Sensor> sensors = null;
		String choice;


		location = loc.getSelectedItem().toString();
		brand = jmarque.getSelectedItem().toString();



		switch(type_Capt.getSelectedItem().toString()) {
		case "-- Selectionner --" :
			type = "-- Selectionner --";
			break;
		case "detecteur de fumee" :
			type = "detecteur_fumee";
			break;
		case "detecteur de mouvement" :
			type = "detecteur_mouvement";
			break;
		case "capteur de lumiere" :
			type ="capteur_lumiere";
			break;
		case "capteur de temperature" :
			type = "capteur_temperature";
			break;
		case "ouverture des portes" : 
			type = "ouverture_porte";
			break;
		case "compteur" : 
			type = "compteur";
			break;
		default :
			type="-- Selectionner --";
			break;
		}




		if (e.getSource()==ok) {


			// ajouter que type + marque et afficher les locs ????
			try {



				if (brand=="-- Selectionner --") {

					Sensor sensorFinal = new Sensor();
					sensorFinal.setType(type);
					sensorFinal.setLocation(location);
					ArrayList<Sensor> sensorsFiltered = new ArrayList();
					ArrayList<String> marques = new ArrayList();
					ArrayList<Double> prices = new ArrayList();
					HashMap <String,Double> ampList = new HashMap();

					sensors = this.sh.test(sensorFinal);


					for(Sensor s : sensors) {
						if (s.getType().equals(type)) {
							if (s.getLocation().equals(location)) {
								System.out.println(s);
								sensorsFiltered.add(s);
							}
						}
					}

					if (sensorsFiltered.isEmpty()) {
						LocError locError = new LocError();
					}

					for(Sensor s : sensorsFiltered) {
						marques.add(s.getBrand());
					}




					for(int i = 0 ; i<marques.size(); i++) {
						double priceBrand = 0;
						for (Sensor s : sensorsFiltered) {
							if(marques.get(i).equals(s.getBrand())) {
								priceBrand = s.getAnual_maintenance_price();
								break;
							}
						}
						ampList.put(marques.get(i), priceBrand);
					}

					System.out.println(ampList);

					String finalprice = "";
					String finalprice2 = "";
					String finalprice3 = "";

					for(Entry<String, Double> entry : ampList.entrySet()) {
						String key = entry.getKey();
						Double value = entry.getValue();

						finalprice+="Coût maintenance : "+value+"€, Marque : "+key+"\n";

					}

					for(Entry<String, Double> entry : ampList.entrySet()) {
						String key = entry.getKey();
						Double value = entry.getValue();

						finalprice2+="Coût maintenance : "+value*5+"€, Marque : "+key+"\n";

					}

					for(Entry<String, Double> entry : ampList.entrySet()) {
						String key = entry.getKey();
						Double value = entry.getValue();

						finalprice3+="Coût maintenance : "+value*10+"€, Marque : "+key+"\n";

					}


					DecimalFormat df = new DecimalFormat();
					df.setRoundingMode(RoundingMode.HALF_UP);




					if (oneYear.isSelected()) {
						if (fiveYears.isSelected() || tenYears.isSelected()) {
							PeriodError pError = new PeriodError();
						}
						else {
							coutOk.setText(finalprice);
						}
					}
					else if (fiveYears.isSelected()) {

						if (oneYear.isSelected() || tenYears.isSelected()) {
							PeriodError pError = new PeriodError();
						}
						else {

							coutOk.setText(finalprice2);
						}
					}

					else if (tenYears.isSelected()) {

						if (oneYear.isSelected() || fiveYears.isSelected()) {
							PeriodError pError = new PeriodError();
						}
						else {
							coutOk.setText(finalprice3);
						}
					}

				}
				
				else if (type=="-- Selectionner --") {
					
					Sensor sensorFinal = new Sensor();
					sensorFinal.setLocation(location);
					sensorFinal.setBrand(brand);
					ArrayList<Sensor> sensorsFiltered = new ArrayList();
					ArrayList<String> types = new ArrayList();
					ArrayList<Double> prices = new ArrayList();
					HashMap <String,Double> ampList = new HashMap();

					sensors = this.sh.test(sensorFinal);


					for(Sensor s : sensors) {
						if (s.getLocation().equals(location)) {
							if (s.getBrand().equals(brand)) {
								System.out.println(s);
								sensorsFiltered.add(s);
							}
						}
					}

					if (sensorsFiltered.isEmpty()) {
						BrandError brandError = new BrandError();
					}

					for(Sensor s : sensorsFiltered) {
						types.add(s.getType());
					}




					for(int i = 0 ; i<types.size(); i++) {
						double priceBrand = 0;
						for (Sensor s : sensorsFiltered) {
							if(types.get(i).equals(s.getType())) {
								priceBrand = s.getAnual_maintenance_price();
								break;
							}
						}
						ampList.put(types.get(i), priceBrand);
					}

					System.out.println(ampList);

					String finalprice = "";
					String finalprice2 = "";
					String finalprice3 = "";

					for(Entry<String, Double> entry : ampList.entrySet()) {
						String key = entry.getKey();
						Double value = entry.getValue();

						finalprice+="Coût maintenance : "+value+"€, Type : "+key+"\n";
						
					}

					for(Entry<String, Double> entry : ampList.entrySet()) {
						String key = entry.getKey();
						Double value = entry.getValue();

						finalprice2+="Coût maintenance : "+value*5+"€, Type : "+key+"\n";

					}

					for(Entry<String, Double> entry : ampList.entrySet()) {
						String key = entry.getKey();
						Double value = entry.getValue();

						finalprice3+="Coût maintenance : "+value*10+"€, Type : "+key+"\n";

					}


					DecimalFormat df = new DecimalFormat();
					df.setRoundingMode(RoundingMode.HALF_UP);




					if (oneYear.isSelected()) {
						if (fiveYears.isSelected() || tenYears.isSelected()) {
							PeriodError pError = new PeriodError();
						}
						else {
							coutOk.setText(finalprice);
						}
					}
					else if (fiveYears.isSelected()) {

						if (oneYear.isSelected() || tenYears.isSelected()) {
							PeriodError pError = new PeriodError();
						}
						else {

							coutOk.setText(finalprice2);
						}
					}

					else if (tenYears.isSelected()) {

						if (oneYear.isSelected() || fiveYears.isSelected()) {
							PeriodError pError = new PeriodError();
						}
						else {
							coutOk.setText(finalprice3);
						}
					}
					
				}
				
				else if (location=="-- Selectionner --") {
					
					Sensor sensorFinal = new Sensor();
					sensorFinal.setType(type);
					sensorFinal.setBrand(brand);
					ArrayList<Sensor> sensorsFiltered = new ArrayList();
					ArrayList<String> locations = new ArrayList();
					ArrayList<Double> prices = new ArrayList();
					HashMap <String,Double> ampList = new HashMap();

					sensors = this.sh.test(sensorFinal);


					for(Sensor s : sensors) {
						if (s.getType().equals(type)) {
							if (s.getBrand().equals(brand)) {
								System.out.println(s);
								sensorsFiltered.add(s);
							}
						}
					}

					if (sensorsFiltered.isEmpty()) {
						BrandError brandError = new BrandError();
					}

					for(Sensor s : sensorsFiltered) {
						locations.add(s.getLocation());
					}




					for(int i = 0 ; i<locations.size(); i++) {
						double priceBrand = 0;
						for (Sensor s : sensorsFiltered) {
							if(locations.get(i).equals(s.getLocation())) {
								priceBrand = s.getAnual_maintenance_price();
								break;
							}
						}
						ampList.put(locations.get(i), priceBrand);
					}

					System.out.println(ampList);

					String finalprice = "";
					String finalprice2 = "";
					String finalprice3 = "";

					for(Entry<String, Double> entry : ampList.entrySet()) {
						String key = entry.getKey();
						Double value = entry.getValue();

						finalprice+="Coût maintenance : "+value+"€, Localisation : "+key+"\n";

					}

					for(Entry<String, Double> entry : ampList.entrySet()) {
						String key = entry.getKey();
						Double value = entry.getValue();

						finalprice2+="Coût maintenance : "+value*5+"€, Localisation : "+key+"\n";

					}

					for(Entry<String, Double> entry : ampList.entrySet()) {
						String key = entry.getKey();
						Double value = entry.getValue();

						finalprice3+="Coût maintenance : "+value*10+"€, Localisation : "+key+"\n";

					}


					DecimalFormat df = new DecimalFormat();
					df.setRoundingMode(RoundingMode.HALF_UP);




					if (oneYear.isSelected()) {
						if (fiveYears.isSelected() || tenYears.isSelected()) {
							PeriodError pError = new PeriodError();
						}
						else {
							coutOk.setText(finalprice);
						}
					}
					else if (fiveYears.isSelected()) {

						if (oneYear.isSelected() || tenYears.isSelected()) {
							PeriodError pError = new PeriodError();
						}
						else {

							coutOk.setText(finalprice2);
						}
					}

					else if (tenYears.isSelected()) {

						if (oneYear.isSelected() || fiveYears.isSelected()) {
							PeriodError pError = new PeriodError();
						}
						else {
							coutOk.setText(finalprice3);
						}
					}
					
				}

				else {

					Sensor sensorFinal = new Sensor();
					sensorFinal.setType(type);
					sensorFinal.setLocation(location);
					sensorFinal.setBrand(brand);
					ArrayList<Sensor> sensorsFiltered = new ArrayList();
					ArrayList<String> marques = new ArrayList();
					ArrayList<Double> prices = new ArrayList();
					HashMap <String,Double> ampList = new HashMap();

					sensors = this.sh.test(sensorFinal);

					for(Sensor s : sensors) {
						if (s.getType().equals(type)) {
							if (s.getLocation().equals(location)) {
								if (s.getBrand().equals(brand)) {
									System.out.println(s);
									sensorsFiltered.add(s);
								}
							}
						}
					}

					if (sensorsFiltered.isEmpty()) {
						LocTypeBrandError loctypebrandError = new LocTypeBrandError();
					}

					for(Sensor s : sensorsFiltered) {
						marques.add(s.getBrand());
					}




					for(int i = 0 ; i<marques.size(); i++) {
						double priceBrand = 0;
						for (Sensor s : sensorsFiltered) {
							if(marques.get(i).equals(s.getBrand())) {
								priceBrand = s.getAnual_maintenance_price();
								break;
							}
						}
						ampList.put(marques.get(i), priceBrand);
					}

					System.out.println(ampList);

					String finalprice = "";
					String finalprice2 = "";
					String finalprice3 = "";

					for(Entry<String, Double> entry : ampList.entrySet()) {
						String key = entry.getKey();
						Double value = entry.getValue();

						finalprice+="Coût maintenance : "+value+"€, Marque : "+key+"\n";

					}

					for(Entry<String, Double> entry : ampList.entrySet()) {
						String key = entry.getKey();
						Double value = entry.getValue();

						finalprice2+="Coût maintenance : "+value*5+"€, Marque : "+key+"\n";

					}

					for(Entry<String, Double> entry : ampList.entrySet()) {
						String key = entry.getKey();
						Double value = entry.getValue();

						finalprice3+="Coût maintenance : "+value*10+"€, Marque : "+key+"\n";

					}


					DecimalFormat df = new DecimalFormat();
					df.setRoundingMode(RoundingMode.HALF_UP);




					if (oneYear.isSelected()) {
						if (fiveYears.isSelected() || tenYears.isSelected()) {
							PeriodError pError = new PeriodError();
						}
						else {
							coutOk.setText(finalprice);
						}
					}
					else if (fiveYears.isSelected()) {

						if (oneYear.isSelected() || tenYears.isSelected()) {
							PeriodError pError = new PeriodError();
						}
						else {

							coutOk.setText(finalprice2);
						}
					}

					else if (tenYears.isSelected()) {

						if (oneYear.isSelected() || fiveYears.isSelected()) {
							PeriodError pError = new PeriodError();
						}
						else {
							coutOk.setText(finalprice3);
						}
					}


				}


			}




			catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}


		}

		else if (e.getSource()==btnTousLesCapteurs) {


			try {



				Sensor sensorFinal = new Sensor();
				sensorFinal.setType(type);
				sensorFinal.setLocation(location);
				double amp = 0;


				sensors = this.sh.test(sensorFinal);

				System.out.println(sensors);

				for(Sensor s : sensors) {
					amp+=s.getAnual_maintenance_price();
				}

				System.out.println(amp);
				DecimalFormat df = new DecimalFormat();
				df.setRoundingMode(RoundingMode.HALF_UP);
				String price = df.format(amp);
				String price2 = df.format(amp*5);
				String price3 = df.format(amp*10);



				if (oneYear.isSelected()) {
					if (fiveYears.isSelected() || tenYears.isSelected()) {
						PeriodError pError = new PeriodError();
					}
					else {
						coutPourTous.setText("Coût total de la maintenance sur 1 an : \n"+price+"€");
					}
				}
				else if (fiveYears.isSelected()) {
					if (oneYear.isSelected() || tenYears.isSelected()) {
						PeriodError pError = new PeriodError();
					}
					else {
						coutPourTous.setText("Coût total de la maintenance sur 5 ans : \n"+price2+"€");
					}
				}

				else if (tenYears.isSelected()) 
					if (oneYear.isSelected() || fiveYears.isSelected()) {
						PeriodError pError = new PeriodError();
					}
					else {
						coutPourTous.setText("Coût total de la maintenance sur 10 ans : \n"+price3+"€");
					}


			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}



	}
}

