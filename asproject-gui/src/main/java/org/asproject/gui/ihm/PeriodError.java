package org.asproject.gui.ihm;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class PeriodError extends JFrame {
	
	public PeriodError() {
		setVisible(true);
		getContentPane().setBackground(new Color(255, 248, 220));
		setBounds(100, 100, 600, 200);
		getContentPane().setLayout(null);
		
		JLabel lblErreur = new JLabel("Erreur");
		lblErreur.setFont(new Font("Century Gothic", Font.PLAIN, 13));
		lblErreur.setBounds(176, 17, 61, 16);
		getContentPane().add(lblErreur);
		
		JLabel lblNewLabel = new JLabel("Il est impossible d’afficher le coût de maintenance sur plusieurs périodes à la fois.");
		lblNewLabel.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		lblNewLabel.setBounds(18, 58, 555, 16);
		getContentPane().add(lblNewLabel);
		
		JLabel lblVeuillezVousAssurer = new JLabel("Merci de n’en sélectionner qu’une, puis de réessayer.");
		lblVeuillezVousAssurer.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		lblVeuillezVousAssurer.setBounds(18, 86, 555, 16);
		getContentPane().add(lblVeuillezVousAssurer);
		
	}

}
