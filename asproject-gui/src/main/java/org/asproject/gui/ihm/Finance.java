package org.asproject.gui.ihm;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class Finance extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JButton btnAjouterUnCapteur;
	private JButton btnAjouterUnePiece;
	private JButton btnCoutMaintenance;
	//private ClientSocket client;

	public Finance() {
		
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 248, 220));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblGrerLaFinance = new JLabel("Gérer la finance des capteurs");
		lblGrerLaFinance.setFont(new Font("Avenir Next", Font.BOLD, 15));
		lblGrerLaFinance.setBounds(126, 39, 239, 17);
		contentPane.add(lblGrerLaFinance);
		
		JLabel lblQueSouhaitezvousFaire = new JLabel("Que souhaitez-vous faire ?");
		lblQueSouhaitezvousFaire.setFont(new Font("Century Gothic", Font.PLAIN, 13));
		lblQueSouhaitezvousFaire.setBounds(142, 107, 183, 17);
		contentPane.add(lblQueSouhaitezvousFaire);
		
		btnAjouterUnCapteur = new JButton("Ajout capteur(s)");
		btnAjouterUnCapteur.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		btnAjouterUnCapteur.setBounds(83, 155, 141, 68);
		contentPane.add(btnAjouterUnCapteur);
		
		btnCoutMaintenance = new JButton("Voir coût maintenance");
		btnCoutMaintenance.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		btnCoutMaintenance.setBounds(251, 155, 141, 68);
		contentPane.add(btnCoutMaintenance);
		
		btnAjouterUnCapteur.addActionListener(this);
		btnCoutMaintenance.addActionListener(this);
		
	}


	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == btnAjouterUnCapteur) {
			AddSensor add = new AddSensor();
		}
		
		else if (e.getSource()== btnCoutMaintenance) {
			Maintenance mtnc = new Maintenance();
		}
		
		
	}
	
	
}
