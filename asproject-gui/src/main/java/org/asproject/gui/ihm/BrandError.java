package org.asproject.gui.ihm;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class BrandError extends JFrame {
	
	public BrandError() {
		setVisible(true);
		getContentPane().setBackground(new Color(255, 248, 220));
		setBounds(100, 100, 400, 200);
		getContentPane().setLayout(null);
		
		JLabel lblErreur = new JLabel("Erreur");
		lblErreur.setFont(new Font("Century Gothic", Font.PLAIN, 13));
		lblErreur.setBounds(176, 17, 61, 16);
		getContentPane().add(lblErreur);
		
		JLabel lblNewLabel = new JLabel("Il n’existe aucun capteur de ce type ayant cette marque.");
		lblNewLabel.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		lblNewLabel.setBounds(18, 58, 365, 16);
		getContentPane().add(lblNewLabel);
		
		JLabel lblVeuillezVousAssurer = new JLabel("Veuillez réessayer.");
		lblVeuillezVousAssurer.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		lblVeuillezVousAssurer.setBounds(18, 86, 365, 16);
		getContentPane().add(lblVeuillezVousAssurer);
	}

}
