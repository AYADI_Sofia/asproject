package org.asproject.gui.ihm;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.asproject.commons.Calculation;
import org.asproject.commons.Sensor;
import org.asproject.gui.socket.SensorHandler;

import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.DefaultComboBoxModel;

public class Construction extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JTextField loc;
	private JTextField achat;
	private JTextField maintenance;
	private JComboBox type;
	private JComboBox brand;
	private JButton add;
	private JButton simul;
	private String st;
	private Double price;
	private Double anual_maintenance_price;
	private int capacity;
	private int garanty;
	private ResultSet rs;
	private PreparedStatement ps;
	private Sensor sensor;
	private JTextField done;
	private double coutachat;
	private double coutmaintenance;
	private JTextField number;
	private SensorHandler sh;


	public Construction() {

		this.sh = new SensorHandler();



		setVisible(true);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 248, 220));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblConstruction = new JLabel("Construction");
		lblConstruction.setFont(new Font("Century Gothic", Font.BOLD, 13));
		lblConstruction.setBounds(191, 25, 101, 17);
		contentPane.add(lblConstruction);

		JLabel lblNomPiece = new JLabel("Nom de la pièce : ");
		lblNomPiece.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblNomPiece.setBounds(21, 64, 101, 16);
		contentPane.add(lblNomPiece);

		loc = new JTextField();
		loc.setColumns(10);
		loc.setBounds(134, 59, 130, 26);
		contentPane.add(loc);

		JLabel lblTypeDeCapteur = new JLabel("Type de capteur :");
		lblTypeDeCapteur.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblTypeDeCapteur.setBounds(21, 92, 119, 29);
		contentPane.add(lblTypeDeCapteur);

		type = new JComboBox();
		type.setModel(new DefaultComboBoxModel(new String[] {"detecteur de fumee", "detecteur de mouvement", "capteur de lumiere", "capteur de temperature", "ouverture des portes", "compteur"}));
		type.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		type.setBounds(21, 123, 100, 27);
		contentPane.add(type);

		brand = new JComboBox();
		brand.setModel(new DefaultComboBoxModel(new String[] {"NewOne", "Insafe", "Steihel", "Brennenstuhl", "Iduino", "Microbot", "PyroControle", "Adafruit", "ArtMotion", "Merkur", "Powermaster", "Silamp"}));
		brand.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		brand.setBounds(169, 123, 100, 27);
		contentPane.add(brand);

		JLabel lblMarque = new JLabel("Marque : ");
		lblMarque.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblMarque.setBounds(190, 98, 59, 16);
		contentPane.add(lblMarque);

		JLabel lblNombre = new JLabel("Nombre :");
		lblNombre.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblNombre.setBounds(335, 98, 67, 16);
		contentPane.add(lblNombre);

		simul = new JButton("Simuler");
		simul.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		simul.setBounds(36, 181, 87, 68);
		contentPane.add(simul);

	/*	add = new JButton("Ajouter");
		add.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		add.setBounds(324, 181, 87, 68);
		contentPane.add(add); */

		JLabel lblCotDachat = new JLabel("Coût d'achat :");
		lblCotDachat.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblCotDachat.setBounds(179, 151, 79, 16);
		contentPane.add(lblCotDachat);

		JLabel lblCotDeMaintenance = new JLabel("Coût de maintenance sur 10 ans :");
		lblCotDeMaintenance.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblCotDeMaintenance.setBounds(134, 192, 189, 16);
		contentPane.add(lblCotDeMaintenance);

		achat = new JTextField();
		achat.setColumns(10);
		achat.setBounds(162, 167, 130, 26);
		contentPane.add(achat);

		maintenance = new JTextField();
		maintenance.setColumns(10);
		maintenance.setBounds(162, 208, 130, 26);
		contentPane.add(maintenance);

		done = new JTextField();
		done.setColumns(10);
		done.setBounds(114, 246, 217, 26);
		contentPane.add(done);

		number = new JTextField();
		number.setColumns(10);
		number.setBounds(335, 121, 59, 26);
		contentPane.add(number);

		simul.addActionListener(this);
		//add.addActionListener(this);
	}



	public void actionPerformed(ActionEvent e) {
		String typecapt;
		String marque;
		String location; 
		String howmuch;
		int numbercapt;
		Sensor sensor = null;



		marque = this.brand.getSelectedItem().toString();
		location = this.loc.getText();
		howmuch = this.number.getText();
		numbercapt = Integer.parseInt(howmuch.trim());
		System.out.println(numbercapt);


		switch(this.type.getSelectedItem().toString()) {
		case "detecteur de fumee" :
			typecapt = "detecteur_fumee";
			break;
		case "detecteur de mouvement" :
			typecapt = "detecteur_mouvement";
			break;
		case "capteur de lumiere" :
			typecapt ="capteur_lumiere";
			break;
		case "capteur de temperature" :
			typecapt = "capteur_temperature";
			break;
		case "ouverture des portes" : 
			typecapt = "ouverture_porte ";
			break;
		case "compteur" : 
			typecapt = "compteur";
			break;
		default :
			typecapt="detecteur_fumee";
			break;
		}


		/*if(e.getSource()==add)
		{

			try {


				Sensor sens = new Sensor();
				sens.setType(typecapt);
				sens.setBrand(marque);

				sensor = this.sh.selectSensorByTypeAndBrand(sens);
				
				
				
				System.out.println(sensor.toString());


				for(int i=0; i<numbercapt; i++) {
					this.sh.insert(sensor);
					System.out.println("ok");
				}

				if (numbercapt>1) {
					this.done.setText("Capteurs ajoutés");
				}
				else {
					this.done.setText("Capteur ajouté");
				}
			}

			catch (Exception e1) {
			} 
		}
		else */if (e.getSource()==simul) {


			try {

				Sensor sensorFinal = new Sensor();
				sensorFinal.setType(typecapt);
				sensorFinal.setBrand(marque);

				sensor = this.sh.selectSensorByTypeAndBrand(sensorFinal);

				double amp = sensor.getAnual_maintenance_price();
				System.out.println(amp);
				double price = sensor.getPrice();

				System.out.println(sensor.toString());

				this.coutachat = (price*numbercapt);
				this.coutachat = Calculation.round(this.coutachat);
				achat.setText("Total : "+this.coutachat);


				this.coutmaintenance = (amp*10)*numbercapt;
				System.out.println(this.coutmaintenance);
				maintenance.setText(""+this.coutmaintenance);



			}
			catch(Exception e2) {
			}


		}
	}



}
