package org.asproject.gui.ihm;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.asproject.commons.Sensor;
import org.asproject.gui.socket.SensorHandler;

public class AddSensor extends JFrame implements ActionListener {


	private JPanel contentPane;
	private JTextField simul;
	public JButton addCapteur;
	private JTextField number;
	private JComboBox jmarque;
	private JComboBox jmarque2;
	private JComboBox jmarque3;
	private JComboBox jloc;
	private JComboBox jloc2;
	private JComboBox jloc3;
	private JComboBox jtypeCapt;
	private JComboBox jtypeCapt2;
	private JComboBox jtypeCapt3;
	private JButton btnSimuler;
	private JTextField done;
	private SensorHandler sh;
	private List<Sensor> sensors;
	private List<Sensor> sensors2;
	private List<Sensor> sensors3;




	public AddSensor() {


		this.sh = new SensorHandler();

		setVisible(true);
		setBounds(200, 200, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 248, 220));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);


		JLabel lblAjouterUnCapteur = new JLabel("Ajouter un capteur");
		lblAjouterUnCapteur.setFont(new Font("Century Gothic", Font.BOLD, 13));
		lblAjouterUnCapteur.setBounds(167, 6, 131, 16);
		contentPane.add(lblAjouterUnCapteur);

		JLabel type_capt = new JLabel("Type de capteur");
		type_capt.setFont(new Font("Century Gothic", Font.BOLD, 11));
		type_capt.setBounds(20, 67, 100, 16);
		contentPane.add(type_capt);

		JLabel lblMarque = new JLabel("Marque");
		lblMarque.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblMarque.setBounds(206, 67, 61, 16);
		contentPane.add(lblMarque);

		jloc = new JComboBox();
		jloc.setModel(new DefaultComboBoxModel(new String[] {"couloir", "toilettes", "salle animation collective ", "salle activite", "salle de soins", "accueil", "centre operationnel"}));
		jloc.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		jloc.setBounds(318, 89, 100, 27);
		contentPane.add(jloc);

		JLabel lblLocalisation = new JLabel("Localisation");
		lblLocalisation.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblLocalisation.setBounds(334, 67, 80, 16);
		contentPane.add(lblLocalisation);

		jmarque = new JComboBox();
		jmarque.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		jmarque.setModel(new DefaultComboBoxModel(new String[] {"NewOne", "Insafe", "Steihel", "Brennenstuhl", "Iduino", "Microbot", "PyroControle", "Adafruit", "ArtMotion", "Merkur", "Powermaster", "Silamp"}));
		jmarque.setBounds(178, 89, 100, 27);
		contentPane.add(jmarque);

		addCapteur = new JButton("Ajouter");
		addCapteur.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		addCapteur.setBounds(318, 180, 87, 68);
		contentPane.add(addCapteur);

		jtypeCapt = new JComboBox();
		jtypeCapt.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		jtypeCapt.setModel(new DefaultComboBoxModel(new String[] {"detecteur de fumee", "detecteur de mouvement", "capteur de lumiere", "capteur de temperature", "ouverture des portes", "compteur"}));
		jtypeCapt.setBounds(20, 89, 104, 27);
		contentPane.add(jtypeCapt);

		JLabel lblCotDajout = new JLabel("Coût d'ajout :");
		lblCotDajout.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblCotDajout.setBounds(188, 180, 79, 16);
		contentPane.add(lblCotDajout);

		simul = new JTextField();
		simul.setBounds(168, 205, 130, 26);
		contentPane.add(simul);
		simul.setColumns(10);

		btnSimuler = new JButton("Simuler");
		btnSimuler.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		btnSimuler.setBounds(62, 180, 87, 68);
		contentPane.add(btnSimuler);

		JLabel lblNumber = new JLabel("Nombre");
		lblNumber.setFont(new Font("Century Gothic", Font.BOLD, 11));
		lblNumber.setBounds(20, 34, 48, 16);
		contentPane.add(lblNumber);

		number = new JTextField();
		number.setColumns(10);
		number.setBounds(80, 29, 42, 26);
		contentPane.add(number);

		done = new JTextField();
		done.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		done.setBounds(86, 256, 293, 16);
		contentPane.add(done);
		done.setColumns(10);

		jtypeCapt2 = new JComboBox();
		jtypeCapt2.setModel(new DefaultComboBoxModel(new String[] {"detecteur de fumee", "detecteur de mouvement", "capteur de lumiere", "capteur de temperature", "ouverture des portes", "compteur"}));
		jtypeCapt2.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		jtypeCapt2.setBounds(20, 117, 104, 27);
		contentPane.add(jtypeCapt2);

		jmarque2 = new JComboBox();
		jmarque2.setModel(new DefaultComboBoxModel(new String[] {"NewOne", "Insafe", "Steihel", "Brennenstuhl", "Iduino", "Microbot", "PyroControle", "Adafruit", "ArtMotion", "Merkur", "Powermaster", "Silamp"}));
		jmarque2.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		jmarque2.setBounds(178, 117, 100, 27);
		contentPane.add(jmarque2);

		jloc2 = new JComboBox();
		jloc2.setModel(new DefaultComboBoxModel(new String[] {"couloir", "toilettes", "salle animation collective ", "salle activite", "salle de soins", "accueil", "centre operationnel"}));
		jloc2.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		jloc2.setBounds(318, 117, 100, 27);
		contentPane.add(jloc2);

		jtypeCapt3 = new JComboBox();
		jtypeCapt3.setModel(new DefaultComboBoxModel(new String[] {"detecteur de fumee", "detecteur de mouvement", "capteur de lumiere", "capteur de temperature", "ouverture des portes", "compteur"}));
		jtypeCapt3.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		jtypeCapt3.setBounds(20, 145, 104, 27);
		contentPane.add(jtypeCapt3);

		jmarque3 = new JComboBox();
		jmarque3.setModel(new DefaultComboBoxModel(new String[] {"NewOne", "Insafe", "Steihel", "Brennenstuhl", "Iduino", "Microbot", "PyroControle", "Adafruit", "ArtMotion", "Merkur", "Powermaster", "Silamp"}));
		jmarque3.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		jmarque3.setBounds(178, 145, 100, 27);
		contentPane.add(jmarque3);

		jloc3 = new JComboBox();
		jloc3.setModel(new DefaultComboBoxModel(new String[] {"couloir", "toilettes", "salle animation collective ", "salle activite", "salle de soins", "accueil", "centre operationnel"}));
		jloc3.setFont(new Font("Century Gothic", Font.PLAIN, 10));
		jloc3.setBounds(318, 145, 100, 27);
		contentPane.add(jloc3);

		btnSimuler.addActionListener(this);
		addCapteur.addActionListener(this);

	}

	boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (Exception e) {
			return false;
		}
		return true;
	}


	public void actionPerformed(ActionEvent e) {
		String type;
		String type2;
		String type3;
		String brand;
		String brand2;
		String brand3;
		String location; 
		String location2;
		String location3;
		Sensor sensor = null;
		Sensor sensor2 = null;
		Sensor sensor3 = null;

		String st_nb = number.getText();
		
		int nb=0;
		
		
		try {
			nb = Integer.parseInt(st_nb.trim());
		}
		catch (Exception exc) {
		}


		brand = jmarque.getSelectedItem().toString();
		brand2 = jmarque2.getSelectedItem().toString();
		brand3 = jmarque3.getSelectedItem().toString();
		location = jloc.getSelectedItem().toString();
		location2 = jloc2.getSelectedItem().toString();
		location3 = jloc3.getSelectedItem().toString();
		String typeCapt = jtypeCapt.getSelectedItem().toString();
		String typeCapt2 = jtypeCapt2.getSelectedItem().toString();
		String typeCapt3 = jtypeCapt3.getSelectedItem().toString();



		switch(typeCapt) {
		case "detecteur de fumee" :
			type = "detecteur_fumee";
			break;
		case "detecteur de mouvement" :
			type = "detecteur_mouvement";
			break;
		case "capteur de lumiere" :
			type ="capteur_lumiere";
			break;
		case "capteur de temperature" :
			type = "capteur_temperature";
			break;
		case "ouverture des portes" : 
			type = "ouverture_porte";
			break;
		case "compteur" : 
			type = "compteur";
			break;
		default :
			type="detecteur_fumee";
			break;
		}

		switch(typeCapt2) {
		case "detecteur de fumee" :
			type2 = "detecteur_fumee";
			break;
		case "detecteur de mouvement" :
			type2 = "detecteur_mouvement";
			break;
		case "capteur de lumiere" :
			type2 ="capteur_lumiere";
			break;
		case "capteur de temperature" :
			type2 = "capteur_temperature";
			break;
		case "ouverture des portes" : 
			type2 = "ouverture_porte ";
			break;
		case "compteur" : 
			type2 = "compteur";
			break;
		default :
			type2 = "detecteur_fumee";
			break;
		}

		switch(typeCapt3) {
		case "detecteur de fumee" :
			type3 = "detecteur_fumee";
			break;
		case "detecteur de mouvement" :
			type3 = "detecteur_mouvement";
			break;
		case "capteur de lumiere" :
			type3 ="capteur_lumiere";
			break;
		case "capteur de temperature" :
			type3 = "capteur_temperature";
			break;
		case "ouverture des portes" : 
			type3 = "ouverture_porte ";
			break;
		case "compteur" : 
			type3 = "compteur";
			break;
		default :
			type3 = "detecteur_fumee";
			break;
		}



		if(e.getSource()==addCapteur)
		{

			try {
				if (nb == 1) {
					Sensor sensorFinal = new Sensor();
					sensorFinal.setType(type);
					sensorFinal.setBrand(brand);
					sensorFinal.setLocation(location);

					this.sh.insert(sensorFinal);
					done.setText("Capteur ajouté");

				}
				else if (nb == 2) {

					Sensor sensorFinal = new Sensor();
					sensorFinal.setType(type);
					sensorFinal.setBrand(brand);
					sensorFinal.setLocation(location);

					Sensor sensorFinal2 = new Sensor();
					sensorFinal2.setType(type2);
					sensorFinal2.setBrand(brand2);
					sensorFinal2.setLocation(location2);

					this.sh.insert(sensorFinal);
					this.sh.insert(sensorFinal2);
					done.setText("Capteurs ajoutés");


				}

				else if (nb == 3) {

					Sensor sensorFinal = new Sensor();
					sensorFinal.setType(type);
					sensorFinal.setBrand(brand);
					sensorFinal.setLocation(location);

					Sensor sensorFinal2 = new Sensor();
					sensorFinal2.setType(type2);
					sensorFinal2.setBrand(brand2);
					sensorFinal2.setLocation(location2);

					Sensor sensorFinal3 = new Sensor();
					sensorFinal3.setType(type3);
					sensorFinal3.setBrand(brand3);
					sensorFinal3.setLocation(location3);

					this.sh.insert(sensorFinal);
					this.sh.insert(sensorFinal2);
					this.sh.insert(sensorFinal3);
					done.setText("Capteurs ajoutés");


				}

				else {
					NumberError nbError = new NumberError();
				}

			}
			catch (Exception e1) {

			}
		}
		else if (e.getSource()==btnSimuler) {

			try {

				Sensor sensorFinal = new Sensor();
				sensorFinal.setType(type);
				sensorFinal.setBrand(brand);
				sensorFinal.setLocation(location);

				Sensor sensorFinal2 = new Sensor();
				sensorFinal2.setType(type2);
				sensorFinal2.setBrand(brand2);
				sensorFinal2.setLocation(location2);

				Sensor sensorFinal3 = new Sensor();
				sensorFinal3.setType(type3);
				sensorFinal3.setBrand(brand3);
				sensorFinal3.setLocation(location3);

				ArrayList<Sensor> testLocType = new ArrayList();
				ArrayList<Sensor> testLocType2 = new ArrayList();
				ArrayList<Sensor> testLocType3 = new ArrayList();

				sensors = new ArrayList();
				sensors2 = new ArrayList();
				sensors3 = new ArrayList();

				sensors = this.sh.test(sensorFinal);
				sensors2 = this.sh.test(sensorFinal2);
				sensors3 = this.sh.test(sensorFinal3);

				for(Sensor s : sensors) {
					if (s.getType().equals(type)) {
						if (s.getLocation().equals(location)) {
							System.out.println(s);
							testLocType.add(s);
						}
					}
				}

				if (testLocType.isEmpty()) {
					LocError locError = new LocError();
				}

				for(Sensor s2 : sensors2) {
					if (s2.getType().equals(type2)) {
						if (s2.getLocation().equals(location2)) {
							System.out.println(s2);
							testLocType2.add(s2);
						}
					}
				}

				if (testLocType2.isEmpty()) {
					LocError locError = new LocError();
				}

				for(Sensor s3 : sensors3) {
					if (s3.getType().equals(type3)) {
						if (s3.getLocation().equals(location3)) {
							System.out.println(s3);
							testLocType3.add(s3);
						}
					}
				}

				if (testLocType3.isEmpty()) {
					LocError locError = new LocError();
				}
				
				
				if (nb==1) {

					Sensor sensorF = new Sensor();
					sensorF.setType(type);
					sensorF.setBrand(brand);
					sensorF.setLocation(location);

					sensor = this.sh.selectSensorByTypeBrandAndLocation(sensorF);

					double price = sensor.getPrice();
					double amp = sensor.getAnual_maintenance_price();


					Double simulation = price+amp;
					DecimalFormat df = new DecimalFormat();
					df.setRoundingMode(RoundingMode.HALF_UP);
					String s = df.format(simulation);
					simul.setText("Total = "+s+"€");

				}

				else if (nb==2) {

					Sensor sensorF = new Sensor();
					sensorF.setType(type);
					sensorF.setBrand(brand);
					sensorF.setLocation(location);

					sensor = this.sh.selectSensorByTypeBrandAndLocation(sensorF);

					double price = sensor.getPrice();
					double amp = sensor.getAnual_maintenance_price();

					Sensor sensorF2 = new Sensor();
					sensorF2.setType(type2);
					sensorF2.setBrand(brand2);
					sensorF2.setLocation(location2);


					sensor2 = this.sh.selectSensorByTypeBrandAndLocation(sensorF2);

					double price2 = sensor2.getPrice();
					double amp2 = sensor2.getAnual_maintenance_price();


					Double simulation = price+amp+price2+amp2;
					DecimalFormat df = new DecimalFormat();
					df.setRoundingMode(RoundingMode.HALF_UP);
					String s = df.format(simulation);
					simul.setText("Total = "+s+"€");

				}

				else if (nb==3) {

					Sensor sensorF = new Sensor();
					sensorF.setType(type);
					sensorF.setBrand(brand);
					sensorF.setLocation(location);

					sensor = this.sh.selectSensorByTypeBrandAndLocation(sensorF);

					double price = sensor.getPrice();
					double amp = sensor.getAnual_maintenance_price();

					Sensor sensorF2 = new Sensor();
					sensorF2.setType(type2);
					sensorF2.setBrand(brand2);
					sensorF2.setLocation(location2);

					sensor2 = this.sh.selectSensorByTypeBrandAndLocation(sensorF2);

					double price2 = sensor2.getPrice();
					double amp2 = sensor2.getAnual_maintenance_price();

					Sensor sensorF3 = new Sensor();
					sensorF3.setType(type3);
					sensorF3.setBrand(brand3);
					sensorF3.setLocation(location3);

					sensor3 = this.sh.selectSensorByTypeBrandAndLocation(sensorF3);

					double price3 = sensor3.getPrice();
					double amp3 = sensor3.getAnual_maintenance_price();


					Double simulation = price+amp+price2+amp2+price3+amp3;
					DecimalFormat df = new DecimalFormat();
					df.setRoundingMode(RoundingMode.HALF_UP);
					String s = df.format(simulation);
					simul.setText("Total = "+s+"€");

				}

				else {
					NumberError nbError = new NumberError();
				}

			} 
			catch(Exception e2) {
			}

		}

	}
}
