package org.asproject.gui.socket;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.asproject.commons.Sensor;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public class SensorHandler {
	private JsonReader reader;
	private JsonWriter writer;
	private Socket sck;
	private Sensor sensor;
	private List<Sensor> sensors;


	public SensorHandler() {

	}

	public void getConnectionFlux() throws UnknownHostException, IOException {
		sck = new Socket("172.31.254.56", 5000);
		//sck = new Socket("localhost", 5000);
		System.out.println("Connected");
		this.reader = new JsonReader(new InputStreamReader(sck.getInputStream(), "UTF-8"));
		this.writer = new JsonWriter(new OutputStreamWriter(sck.getOutputStream(), "UTF-8"));
	}

	public void closeConnectionFlux() throws IOException {
		sck.close();
		reader.close();
		writer.close();
		System.out.println("Disconnected");
	}

	public Sensor selectSensorByTypeBrandAndLocation(Sensor sensor) throws UnknownHostException, IOException {

		getConnectionFlux();
		Sensor sensorRcv = null;

		try {
			//On envoie la requete et l'objet
			writer.beginObject();

			writer.name("request");
			writer.value("select Sensor By Type Brand And Location");


			writer.name("object");
			writer.value(new Gson().toJson(sensor));

			writer.endObject();
			writer.flush();

			System.out.println("ça a été envoyé");

			//On lit la réponse

			reader.beginObject();

			while(reader.hasNext()) {
				String etiquette = reader.nextName();
				if (etiquette.equals("sensor")){

					String object = reader.nextString();
					sensorRcv = new Gson().fromJson(object, Sensor.class);

				}
				else if (etiquette.equals("error")){
					String message = reader.nextString();
					System.out.println(message);
				}

			}

			reader.endObject();

			System.out.println("résultat :" +sensorRcv.toString());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	finally {

			try {
				closeConnectionFlux();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return sensorRcv;

	}

	public Sensor selectSensorByTypeAndLocation(Sensor sensor) throws UnknownHostException, IOException {

		getConnectionFlux();
		Sensor sensorRcv2 = null;

		try {
			//On envoie la requete et l'objet
			writer.beginObject();

			writer.name("request");
			writer.value("select Sensor By Type And Location");


			writer.name("object");
			writer.value(new Gson().toJson(sensor));

			writer.endObject();
			writer.flush();

			System.out.println("ça a été envoyé");

			//On lit la réponse

			reader.beginObject();

			while(reader.hasNext()) {
				String etiquette = reader.nextName();
				if (etiquette.equals("sensor")){

					String object = reader.nextString();
					sensorRcv2 = new Gson().fromJson(object, Sensor.class);

				}
				else if (etiquette.equals("error")){
					String message = reader.nextString();
					System.out.println("error" + message);
				}

			}

			reader.endObject();

			System.out.println("résultat :" +sensorRcv2.toString());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		return sensorRcv2;
	}

	public List<Sensor> test(Sensor sensor) throws UnknownHostException, IOException {

			System.out.println("1");
			getConnectionFlux();
			System.out.println("2");
			sensors = new ArrayList();

			try {
				//On envoie la requete et l'objet
				writer.beginObject();

				writer.name("request");
				writer.value("test");


				writer.name("object");
				writer.value(new Gson().toJson(sensor));

				writer.endObject();
				writer.flush();

				System.out.println("ça a été envoyé");

				//On lit la réponse

				reader.beginObject();

				while(reader.hasNext()) {
					String etiquette = reader.nextName();
					if (etiquette.equals("sensorList")){
						//System.out.println("yo");
						String object = reader.nextString();
						//System.out.println(object);
						sensors.add(new Gson().fromJson(object, Sensor.class));

					}
					else if (etiquette.equals("error")){
						String message = reader.nextString();
						System.out.println("error : "+message);
					}

				}

				reader.endObject();

				//System.out.println("résultat :" +object);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		finally {

			try {
				closeConnectionFlux();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return sensors;
	}

	

	public Sensor selectSensorByTypeAndBrand(Sensor sensor) throws UnknownHostException, IOException {

		getConnectionFlux();
		Sensor sensorRcv3 = null;

		try {
			//On envoie la requete et l'objet
			writer.beginObject();

			writer.name("request");
			writer.value("select Sensor By Type And Brand");


			writer.name("object");
			writer.value(new Gson().toJson(sensor));

			writer.endObject();
			writer.flush();

			System.out.println("babybel");
			System.out.println("ça a été envoyé");

			//On lit la réponse

			reader.beginObject();

			while(reader.hasNext()) {
				String etiquette = reader.nextName();
				if (etiquette.equals("sensor")){

					String object = reader.nextString();
					sensorRcv3 = new Gson().fromJson(object, Sensor.class);

				}
				else if (etiquette.equals("error")){
					String message = reader.nextString();
					System.out.println(message);
				}

			}

			reader.endObject();

			System.out.println("résultat :" +sensorRcv3.toString());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {

			try {
				closeConnectionFlux();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return sensorRcv3;

	}

	public Sensor insert(Sensor sensor) throws UnknownHostException, IOException {

		getConnectionFlux();
		Sensor sensorRcv = null;

		try {
			//On envoie la requete et l'objet
			writer.beginObject();

			writer.name("request");
			writer.value("insert Sensor");


			writer.name("object");
			writer.value(new Gson().toJson(sensor));

			writer.endObject();
			writer.flush();

			System.out.println("ça a été envoyé");

			//On lit la réponse

			reader.beginObject();

			while(reader.hasNext()) {
				String etiquette = reader.nextName();
				if (etiquette.equals("successful insertion")){

					String object = reader.nextString();
					System.out.println(object);

				}
				else if (etiquette.equals("error")){
					String message = reader.nextString();
					System.out.println(message);
				}

				System.out.println("on a lu la réponse");
			}

			reader.endObject();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {

			try {
				closeConnectionFlux();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return sensorRcv;

	}



}
