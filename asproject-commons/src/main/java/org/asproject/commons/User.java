package org.asproject.commons;

public class User {
	private int id;
	private String email;
	private String password;
	private String lastname;
	private String firstname;
	private boolean isFinancial;
	
	public User(int id, String email, String pw, String ln, String fn, boolean financial) {
		this.setId(id);
		this.setEmail(email);
		this.setPassword(pw);
		this.setLastName(ln);
		this.setFirstName(fn);
		this.setIsFinancial(financial);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastName() {
		return lastname;
	}

	public void setLastName(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstName() {
		return firstname;
	}

	public void setFirstName(String firstname) {
		this.firstname = firstname;
	}

	public boolean getIsFinancial() {
		return isFinancial;
	}

	public void setIsFinancial(boolean isFinancial) {
		this.isFinancial = isFinancial;
	}
	
	

}
