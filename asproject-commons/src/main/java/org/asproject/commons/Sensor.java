package org.asproject.commons;

public class Sensor {
	

	private String type;
	private String brand;
	private Double price;
	//private int number;
	private int capacity;
	private int operating_time;
	private String localisation;
	private Double anual_maintenance_price;
	
	public Sensor(String type, String bd, double price, int capacity, int opt, String loc, double amp) {
		this.setType(type);
		this.setBrand(bd);
		this.setPrice(price);
		this.setCapacity(capacity);
		this.setOperating_time(opt);
		this.setLocation(loc);
		this.setAnual_maintenance_price(amp);
	}
	

	public Sensor() {
		// TODO Auto-generated constructor stub
	}


	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getOperating_time() {
		return operating_time;
	}

	public void setOperating_time(int operating_time) {
		this.operating_time = operating_time;
	}

	/*public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}*/

	public double getAnual_maintenance_price() {
		return anual_maintenance_price;
	}

	public void setAnual_maintenance_price(double anual_maintenance_price) {
		this.anual_maintenance_price = anual_maintenance_price;
	}

	public String getLocation() {
		return localisation;
	}

	public void setLocation(String localisation) {
		this.localisation = localisation;
	}
	
	@Override
	public String toString() {
		return "Sensor [type=" + type + ", brand=" + brand + ", price=" + price + ", capacity=" + capacity
				+ ", operating_time=" + operating_time + ", localisation=" + localisation + ", anual_maintenance_price="
				+ anual_maintenance_price + "]";
	}
}
