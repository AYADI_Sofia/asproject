package org.asproject.commons;

import java.math.BigDecimal;

public class Calculation {
	
	public static double round (double a) {
		BigDecimal bd = new BigDecimal(a);
		bd = bd.setScale(2,  BigDecimal.ROUND_DOWN);
		a = bd.doubleValue();
		return a;
	}
	
	
}
