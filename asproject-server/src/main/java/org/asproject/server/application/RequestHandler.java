package org.asproject.server.application;



import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;


import org.asproject.commons.Sensor;
import org.asproject.server.dao.DAOException;
import org.asproject.server.dao.SensorDAO;
import org.asproject.server.dao.SensorDAOImplement;
import org.asproject.server.pool.ConnectionPool;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;



public class RequestHandler implements Runnable {
	private JsonReader reader;
	private JsonWriter writer;
	private Socket sck;
	private Connection co;
	private SensorDAOImplement sdi;

	public RequestHandler(Socket sck, Connection co) throws IOException {
		super();
		this.co=co;
		this.sck = sck;
		this.reader = new JsonReader(new InputStreamReader(sck.getInputStream(), "UTF-8"));
		this.writer = new JsonWriter(new OutputStreamWriter(sck.getOutputStream(), "UTF-8"));
		sdi = new SensorDAOImplement(co);
	}

	public void run() {

		System.out.println("Ca run");

		String request = null;
		String object = null;
		Sensor sensor = null;
		Sensor sensorSend = null;
		ArrayList<Sensor> sensors = null;
		
		try {

			reader.beginObject();

			while(reader.hasNext()) {
				System.out.println("le while tourne");
				String etiquette = reader.nextName();
				if (etiquette.equals("request")) {
					request = reader.nextString();
				}
				else if (etiquette.equals("object")) {
					object = reader.nextString();	
					sensor = new Gson().fromJson(object, Sensor.class);

				}
				else {
					reader.skipValue();
				}

				System.out.println(etiquette);
				System.out.println(request);
			}

			reader.endObject();

			System.out.println("Objet reçu :"+sensor.toString());


			if (request.equals("select Sensor By Type Brand And Location")) {

				sensorSend = sdi.selectSensorByTypeBrandAndLocation(sensor.getType(), sensor.getBrand(), sensor.getLocation());
				//System.out.println(sensorSend.toString());


				writer.beginObject();

				if (sensorSend != null) {
					writer.name("sensor");
					writer.value(new Gson().toJson(sensorSend));
				}
				else {
					writer.name("error");
					writer.value("sensor not found");
				}

				writer.endObject();
				writer.flush();

				//System.out.println("Objet envoyé :" +sensorSend.toString());
			}

			else if (request.equals("select Sensor By Type And Location")) {

				sensorSend = sdi.selectSensorByTypeAndLocation(sensor.getType(), sensor.getLocation());
				System.out.println(sensorSend.toString());


				writer.beginObject();

				if (sensorSend != null) {
					writer.name("sensor");
					writer.value(new Gson().toJson(sensorSend));
				}
				else {
					writer.name("error");
					writer.value("sensor not found");
				}

				writer.endObject();
				writer.flush();

				System.out.println("Objet envoyé :" +sensorSend.toString());
			}
			
			else if (request.equals("select Sensor By Type And Brand")) {

				sensorSend = sdi.selectSensorByTypeAndBrand(sensor.getType(), sensor.getBrand());
				System.out.println(sensorSend.toString());


				writer.beginObject();

				if (sensorSend != null) {
					writer.name("sensor");
					writer.value(new Gson().toJson(sensorSend));
				}
				else {
					writer.name("error");
					writer.value("sensor not found");
				}

				writer.endObject();
				writer.flush();

				System.out.println("Objet envoyé :" +sensorSend.toString());
			}
			
			else if (request.equals("test")) {
				System.out.println("on est là");
				sensors = sdi.test();
				System.out.println(sensors.toString());


				writer.beginObject();

				if (!sensors.isEmpty()) {
					for (Sensor s : sensors) {
						writer.name("sensorList");
						writer.value(new Gson().toJson(s));
					}
				}
				else {
					writer.name("error");
					writer.value("Sensors list is empty");
				}

				writer.endObject();
				writer.flush();

				//System.out.println("Objet envoyé :" +str1);
			}
			
			
			else if (request.equals("insert Sensor")) {

				sensorSend = sdi.selectSensorByTypeAndLocation(sensor.getType(), sensor.getLocation());
				System.out.println(sensorSend.toString());
				sdi.insert(sensorSend);


				writer.beginObject();

				if (sensorSend != null) {
					writer.name("successful insertion");
					writer.value(new Gson().toJson("Capteur ajouté"));
				}
				else {
					writer.name("error");
					writer.value("sensor not found");
				}

				writer.endObject();
				writer.flush();
			}
			
			//System.out.println("Ca c'est ok");


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*else if (request.equals("select Sensor By Type And Brand")) {

			sensorSend = sdi.selectSensorByTypeAndBrand(sensor.getType(), sensor.getBrand());
			System.out.println(sensorSend.toString());


			writer.beginObject();

			if (sensorSend != null) {
				writer.name("sensor");
				writer.value(new Gson().toJson(sensorSend));
			}
			else {
				writer.name("error");
				writer.value("sensor not found");
			}

			writer.endObject();
			writer.flush();

			System.out.println("Objet envoyé :" +sensorSend.toString());
		}*/
		

		finally {
			try {
				reader.close();
				//writer.close();
				ConnectionPool.connectionToPool(co);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}
}


