package org.asproject.server.application;

import java.io.IOException;
import org.asproject.server.socket.SocketServer;


public class App 
{


	public static void main( String[] args )
	{


		System.out.println( "Server instantiation..." );
		try {
			SocketServer socketServer = new SocketServer(5000);
			socketServer.go();


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
