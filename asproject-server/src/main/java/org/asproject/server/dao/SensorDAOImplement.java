package org.asproject.server.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.List;
import org.asproject.commons.Sensor;


public class SensorDAOImplement implements SensorDAO{


	private Connection connection;
	private Statement ordre;
	private String st;


	public SensorDAOImplement(Connection co) {
		this.connection=co;
	}


	public Sensor selectSensorByTypeBrandAndLocation(String type, String brand, String location) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		Sensor sensor = null;


		this.st = "select * from capteurs where nom_capteur='"+type+"' and marque='"+brand+"' and localisation ='"+location+"' ";
		try {
			ps = connection.prepareStatement(this.st);
			rs = ps.executeQuery();
			while(rs.next()) {
				double price = rs.getDouble("prix");
				int capacity = rs.getInt("portee"); 
				int garanty = rs.getInt("duree_garantie");
				double anual_maintenance_price = rs.getDouble("cout_maintenance_annee");
				sensor = new Sensor(type, brand, price, capacity, garanty, location, anual_maintenance_price);
			}
		} 

		catch (Exception e1) {
		}

		return sensor;

	}

	public Sensor selectSensorByTypeAndLocation(String type,String location) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		Sensor sensor2 = null;

		this.st = "select * from capteurs where nom_capteur='"+type+"' and localisation='"+location+"'";
		try {
			ps = connection.prepareStatement(this.st);
			rs = ps.executeQuery();

			while(rs.next()) {
				String brand = rs.getString("marque");
				double price = rs.getDouble("prix");
				int capacity = rs.getInt("portee"); 
				int garanty = rs.getInt("duree_garantie");
				double anual_maintenance_price = rs.getDouble("cout_maintenance_annee");
				sensor2 = new Sensor(type, brand, price, capacity, garanty, location, anual_maintenance_price);
			}
		}
		catch(Exception exc) {}


		return sensor2;
	}
	
	
	public ArrayList<Sensor> test() {
		System.out.println("yoooo");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Sensor sensor2 = null;
		ArrayList<Sensor> sensors = new ArrayList();
		System.out.println("behhhhh");
		this.st = "select * from capteurs";
		System.out.println("youhouu");
		try {
			ps = connection.prepareStatement(this.st);
			rs = ps.executeQuery();
			System.out.println(rs.next());
			while(rs.next()) {
				String type = rs.getString("nom_capteur");
				String brand = rs.getString("marque");
				double price = rs.getDouble("prix");
				int capacity = rs.getInt("portee"); 
				int garanty = rs.getInt("duree_garantie");
				String location = rs.getString("localisation");
				double anual_maintenance_price = rs.getDouble("cout_maintenance_annee");
				System.out.println("coucou");
				sensor2 = new Sensor(type, brand, price, capacity, garanty, location, anual_maintenance_price);
				System.out.println(sensor2);
				sensors.add(sensor2);
			}
		}
		catch(Exception exc) {
			System.out.println(exc.getMessage());
		}

		System.out.println(sensors);

		return sensors;
	}

	public Sensor selectSensorByTypeAndBrand(String type, String brand) {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		Sensor sensor3 = null;

		this.st = "select * from capteurs where nom_capteur='"+type+"' and marque='"+brand+"'";
		try {
			ps = connection.prepareStatement(this.st);
			rs = ps.executeQuery();

			while(rs.next()) {
				double price = rs.getDouble("prix");
				int capacity = rs.getInt("portee"); 
				int garanty = rs.getInt("duree_garantie");
				String location = rs.getString("localisation");
				double anual_maintenance_price = rs.getDouble("cout_maintenance_annee");
				sensor3 = new Sensor(type, brand, price, capacity, garanty, location, anual_maintenance_price);
			}
		}
		catch(Exception exc) {}


		return sensor3;

	}
	

	public void insert(Sensor sensor) throws DAOException {
		String type = sensor.getType();
		String brand = sensor.getBrand();
		double price = sensor.getPrice();
		int capacity = sensor.getCapacity();
		int duration = sensor.getOperating_time();
		String loc = sensor.getLocation();
		double maint_price = sensor.getAnual_maintenance_price();

		try {
			ordre = connection.createStatement();
		} catch (SQLException ex) {
			Logger.getLogger(SensorDAOImplement.class.getName()).log(Level.SEVERE, null, ex);
		}

		String all="insert into capteurs(nom_capteur, marque, prix, portee, duree_garantie, localisation, cout_maintenance_annee) values ('"+type+"', '"+brand+"', '"+price+"', '"+capacity+"', '"+duration+"', '"+loc+"', '"+maint_price+"')";

		try {
			ordre.executeUpdate(all);
		} catch (SQLException ex) {
			Logger.getLogger(SensorDAOImplement.class.getName()).log(Level.SEVERE, null, ex);
		}
	}


	public void delete(int id) throws DAOException {

		try {
			ordre = connection.createStatement();
		} catch (SQLException ex) {
			Logger.getLogger(SensorDAOImplement.class.getName()).log(Level.SEVERE, null, ex);
		}

		System.out.println("Avant");
		String all="delete from capteurs where id='"+id+"'";

		try {
			ordre.executeUpdate(all);
		} catch (SQLException ex) {
			Logger.getLogger(SensorDAOImplement.class.getName()).log(Level.SEVERE, null, ex);
		}

		System.out.println("Après");


	}

	/*public void update(Sensor sensor) throws DAOException {

	}*/

	public void find(String type, String brand) throws DAOException {
		/*try {
			ordre = connection.createStatement();
		} catch (SQLException ex) {
			Logger.getLogger(SensorDAOImplement.class.getName()).log(Level.SEVERE, null, ex);
		}*/

		String all = "Select localisation from capteurs where nom_capteur='"+type+"' and marque='"+brand+"'";
		String loc ="";
		try {
			PreparedStatement ps = null;
			ps = connection.prepareStatement(all);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				System.out.println(rs.getString("localisation"));
				loc += rs.getString("localisation")+ "\n";
			}
		} catch (SQLException ex) {
			Logger.getLogger(SensorDAOImplement.class.getName()).log(Level.SEVERE, null, ex);
		}

	}
	
	/*public String test(String type, String location) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Sensor sensor2 = null;

		this.st = "select * from capteurs where nom_capteur='"+type+"' and localisation='"+location+"'";
		try {
			ps = connection.prepareStatement(this.st);
			rs = ps.executeQuery();

			while(rs.next()) {
				String brand = rs.getString("marque");
				double anual_maintenance_price = rs.getDouble("cout_maintenance_annee");
				String str1 += brand + " " + anual_maintenance_price + "\n";
			}
		}
		catch(Exception exc) {}


		return str1;
	}*/





}
