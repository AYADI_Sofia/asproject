package org.asproject.server.dao;

import org.asproject.commons.Sensor;

import java.util.ArrayList;


public interface SensorDAO {
	
	Sensor selectSensorByTypeBrandAndLocation(String type, String brand, String location);
	Sensor selectSensorByTypeAndLocation(String type, String location);
	Sensor selectSensorByTypeAndBrand(String type, String brand);
	ArrayList<Sensor> test();
	//String costOfMaintainingAllSensors();
	void insert(Sensor sensor) throws DAOException;
	void delete(int id) throws DAOException;
	//void update(Sensor sensor) throws DAOException;
	void find(String type, String brand) throws DAOException;

}
