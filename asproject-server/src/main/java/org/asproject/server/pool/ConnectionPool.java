package org.asproject.server.pool;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ConnectionPool {
	
	
	private final static ArrayList<Connection> pool = new ArrayList<Connection>();
	
	
	public ConnectionPool() {
		int nbConnection=10;
		try {
			Class.forName("org.postgresql.Driver");
			System.out.println("Driver ok");
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		}
		
		for (int i = 0 ; i < nbConnection ; i++) {
			
			try {
				
				 Connection con = null;
				    ResultSet resultats = null;
				    String requete = "";
				    
				//Connection co = DriverManager.getConnection("jdbc:postgresql://localhost/PDSVamms", "postgres", "ahaedh94");
				Connection co = DriverManager.getConnection("jdbc:postgresql://10.10.10.2:5432/DBASProject", "postgres", "asproject");
				pool.add(co);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		System.out.println("Création effectuée : " + pool.size());
	}
	
	
	public void closeAllConnection(){
		for (int i=0; i < pool.size(); i++){
			try {
				pool.get(i).close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			pool.remove(i);
		}
		System.out.println("Toutes les connexions sont fermées.");
	}
	

	public synchronized static Connection getConnectionPool() {
		if(pool.isEmpty()) {
			System.out.println("Il n'y a pas de connexion disponible");
			return null;
		}
		
		System.out.println(pool.size()-1);
		return pool.remove(pool.size()-1);
	}
	
	
	public synchronized static void connectionToPool(Connection co) {
		pool.add(co);
		System.out.println(pool.size());
	}
       
	public static void main(String[] args) {
		ConnectionPool cp = new ConnectionPool();
		
	}
	
       
}