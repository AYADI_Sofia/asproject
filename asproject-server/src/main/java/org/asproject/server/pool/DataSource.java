package org.asproject.server.pool;


import java.io.IOException;
import java.net.SocketException;
import java.sql.Connection;
public class DataSource {
	//attribut
	private static ConnectionPool cp = new ConnectionPool();

	public DataSource(){
	}

	//methodes

	public static Connection getConnection() throws Exception {
		return cp.getConnectionPool();
	}

	public static void connectionToPool(Connection a) {
		cp.connectionToPool(a);
	}

	public static void closeConnections() {
		cp.closeAllConnection();
	}





}


