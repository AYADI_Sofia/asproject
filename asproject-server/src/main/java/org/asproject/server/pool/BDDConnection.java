package org.asproject.server.pool;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.asproject.server.dao.SensorDAOImplement;

public class BDDConnection  {
	
	private static ConnectionPool cp = new ConnectionPool();
	
	public static Connection getConnection() {
		Connection co = cp.getConnectionPool();
		System.out.println("Connecté à la bdd");
		return co;
	}	
}
