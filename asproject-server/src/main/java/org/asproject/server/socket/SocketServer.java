package org.asproject.server.socket;


import java.io.IOException;
import java.io.InputStream;
import java.net.*;
import java.sql.Connection;

import org.asproject.commons.Sensor;
import org.asproject.server.application.RequestHandler;
import org.asproject.server.dao.SensorDAOImplement;
import org.asproject.server.pool.BDDConnection;


public class SocketServer {
	private int port;
	private ServerSocket srv;
	private static BDDConnection bddCo;
	private SensorDAOImplement sdi;

	public SocketServer(int port) throws IOException {
		this.port = port;

	}

	public void go() {

		//bddCo = new BDDConnection();

		try {
			srv = new ServerSocket(this.port);
			bddCo = new BDDConnection();
		} catch (IOException e) {
			e.printStackTrace();
		}

		while(true) {
			try {
				
				System.out.println("Waiting for a connection...");
				Socket client = srv.accept();
				Connection c = bddCo.getConnection();
				sdi = new SensorDAOImplement(c);

				if (c!=null) {

					/*Sensor sensor = sdi.selectSensorByTypeAndLocation("detecteur_fumee", "couloir");
					System.out.println(sensor.toString());*/
					
					System.out.println("A client is connected : " +client.getInetAddress());

					RequestHandler rq = new RequestHandler(client, c);
					rq.run();

				}



			}

			/*catch(Exception e) {


			}*/

			catch (IOException ex) {
				System.out.println("erreur serveur");

			}


		}
	}


}
